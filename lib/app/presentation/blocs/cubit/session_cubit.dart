import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../domain/models/user.dart';
import '../../../domain/usecases/session/session_usecases.dart';
import 'session_state.dart';

class SessionCubit extends Cubit<SessionState> {
  SessionCubit({required this.sessionUsecases}) : super(const SessionState());
  final SessionUsecases sessionUsecases;
  bool isLoading = false;

  /// The method returns a boolean that depends if user is logged
  Future<bool> signInGoogle() async {
    try {
      final User? user = await sessionUsecases.signIn();
      if (user == null) return false;
      setUser(user);
      return true;
    } catch (e) {
      return false;
    }
  }

  Future<void> updateUser(User user) async {
    try {
      if (isLoading) return;
      isLoading = true;
      // update user in database
      await sessionUsecases.updateUser(user);
      // change the global state user
      setUser(user);
      isLoading = false;
    } catch (e) {
      return;
    }
  }

  /// The method returns boolean if it is authenticated
  Future<bool> isAuthenticated() async {
    // check if the token exists in the local storage.
    final bool isToken = await _existsTokenUserLocal();
    if (!isToken) return false;
    // If the token exists, query the database based on the user id of the token.
    final bool existsInDatabase = await _existsInDatabaseUser();
    if (!existsInDatabase) return false;
    return true;
  }

  Future<bool> _existsInDatabaseUser() async {
    final User? user = await sessionUsecases.getCurrentUser();
    if (user == null) return false;

    //  set user in the global state session
    setUser(user);
    return true;
  }

  Future<bool> _existsTokenUserLocal() {
    return sessionUsecases.existsToken();
  }

  /// The method set user in the state
  void setUser(User user) {
    emit(SessionState(user: user));
  }

  /// sign out ( Clean data local storage and sign off in google)
  Future<void> signOut() async {
    await sessionUsecases.signOut();
  }
  //
}
