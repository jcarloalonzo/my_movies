import '../../domain/datasources/movies_datasource.dart';
import '../either/either.dart';
import '../entities/response/movie_response.dart';
import '../entities/response/trending_response.dart';
import '../services/remote/tracktv_api.dart';

class TrackTvDatasourceImpl implements MoviesDatasource {
  TrackTvDatasourceImpl({required this.api});

  final TrackTvAPI api;
  @override
  Future<Either<String, List<Movie>>> getPopular({int page = 1}) async {
    final response = await api.getPopular(page);
    return response.when(
        left: (failure) => Either.left(failure),
        right: (popularMovies) => Either.right(popularMovies));
  }

  @override
  Future<Either<String, List<TrendingMovie>>> getTendring(
      {int page = 1}) async {
    final response = await api.getTrending(page);
    return response.when(
        left: (failure) => Either.left(failure),
        right: (popularMovies) => Either.right(popularMovies));
  }
}
