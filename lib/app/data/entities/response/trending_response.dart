import 'dart:convert';

import 'movie_response.dart';

class TrendingMovie {
  TrendingMovie({
    required this.watchers,
    required this.movie,
  });

  factory TrendingMovie.fromRawJson(String str) =>
      TrendingMovie.fromJson(json.decode(str));

  factory TrendingMovie.fromJson(Map<String, dynamic> json) => TrendingMovie(
        watchers: json['watchers'],
        movie: Movie.fromJson(json['movie']),
      );
  int watchers;
  Movie movie;

  String toRawJson() => json.encode(toJson());

  Map<String, dynamic> toJson() => {
        'watchers': watchers,
        'movie': movie.toJson(),
      };
}
