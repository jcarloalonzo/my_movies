import 'package:flutter/material.dart';

import '../../../config/constants/palette.dart';
import 'custom_text_field.dart';

class MyTextField extends StatelessWidget {
  const MyTextField({
    super.key,
    this.icon,
    this.onTap,
    this.onTapTextField,
    this.onSubmit,
    this.onChanged,
    this.controller,
    this.textInputType = TextInputType.text,
    this.maxLines = 1,
    this.hintText,
    this.textAlign = TextAlign.start,
    this.iconSuffix,
    this.onTapSuffix,
    this.height = 50,
    this.textCapitalization = TextCapitalization.none,
  });
  final IconData? icon;
  final IconData? iconSuffix;
  final VoidCallback? onTapSuffix;
  final VoidCallback? onTap;
  final VoidCallback? onTapTextField;
  final Function(String)? onSubmit, onChanged;
  final TextEditingController? controller;
  final TextInputType textInputType;
  final int maxLines;
  final String? hintText;
  final TextAlign textAlign;

  final TextCapitalization textCapitalization;
  final double height;
//
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        alignment: Alignment.center,
        height: height,
        decoration: BoxDecoration(
            border: Border.all(
              color: Palette.primaryColor,
              width: 0.7,
            ),
            borderRadius: const BorderRadius.all(
              Radius.circular(6),
            ),
            color: Colors.white),
        child: CustomTextField(
          controller: controller,
          hintText: hintText,
          maxLines: maxLines,
          onChanged: onChanged,
          onTap: onTapTextField,
          onSubmit: onSubmit,
          textCapitalization: textCapitalization,
          textInputType: textInputType,
          textAlign: textAlign,
        ),
      ),
    );
  }
}
