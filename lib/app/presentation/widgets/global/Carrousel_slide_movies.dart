import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

import '../../../../generated/translations.g.dart';
import '../../../config/constants/palette.dart';
import '../../../config/helpers/helpers.dart';
import '../../../data/entities/response/movie_response.dart';
import '../../../data/entities/response/trending_response.dart';
import '../../pages/content_detail/content_detail_page.dart';

class CarrouselSlideMovies extends StatelessWidget {
  const CarrouselSlideMovies({
    super.key,
    required this.trendingMovies,
  });
  final List<TrendingMovie> trendingMovies;
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 160,
      child: CarouselSlider.builder(
        itemCount: trendingMovies.length,
        itemBuilder: (context, index, realIndex) {
          final TrendingMovie trendingMovie = trendingMovies[index];
          return GestureDetector(
              child: _MovieCard(
            trendingMovie: trendingMovie,
            onTap: () {
              context.push(ContentDetailPage.route, extra: trendingMovie.movie);
            },
          ));
        },
        options: CarouselOptions(
          height: 200,
          aspectRatio: 16 / 9,
          viewportFraction: 0.7,
          initialPage: 0,
          enableInfiniteScroll: true,
          reverse: false,
          autoPlay: true,
          autoPlayInterval: const Duration(seconds: 3),
          autoPlayAnimationDuration: const Duration(milliseconds: 1200),
          autoPlayCurve: Curves.fastOutSlowIn,
          enlargeCenterPage: true,
          enlargeFactor: 0.3,
          scrollDirection: Axis.horizontal,
          pageSnapping: true,
        ),
      ),
    );
  }
}

class _MovieCard extends StatelessWidget {
  const _MovieCard({required this.trendingMovie, this.onTap});
  final TrendingMovie trendingMovie;
  final VoidCallback? onTap;
  @override
  Widget build(BuildContext context) {
    final Movie movie = trendingMovie.movie;
    final TextTheme textTheme = Theme.of(context).textTheme;
    return GestureDetector(
      onTap: onTap,
      child: Card(
        shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(12)),
            side: BorderSide(color: Palette.primaryColor, width: 0.5)),
        elevation: 6,
        clipBehavior: Clip.hardEdge,
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Expanded(
                child: Text(
                  movie.title,
                  style: textTheme.bodyMedium?.copyWith(
                    color: Palette.primaryColor,
                    fontSize: 19,
                    fontWeight: FontWeight.w600,
                  ),
                  maxLines: 3,
                  overflow: TextOverflow.ellipsis,
                ),
              ),
              Row(
                children: <Widget>[
                  Icon(
                    Icons.remove_red_eye_outlined,
                    color: Colors.grey[800],
                  ),
                  const SizedBox(width: 5),
                  Text(
                    Helpers.compactNumber(trendingMovie.watchers),
                    style: textTheme.bodyMedium?.copyWith(
                      color: Colors.grey[800],
                      fontSize: 16,
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                ],
              ),
              Text(
                texts.label.watchingNow,
                style: textTheme.bodyMedium?.copyWith(
                  color: Colors.grey[800],
                  fontSize: 13,
                  fontWeight: FontWeight.w500,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
