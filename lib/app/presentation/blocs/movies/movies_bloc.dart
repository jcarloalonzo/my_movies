import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../generated/translations.g.dart';
import '../../../data/entities/response/movie_response.dart';
import '../../../data/entities/response/trending_response.dart';
import '../../../domain/usecases/movies/movies_usecases.dart';
import 'movies_state.dart';

part 'movies_event.dart';

class MoviesBloc extends Bloc<MoviesEvent, MoviesState> {
  MoviesBloc({required this.moviesUsecases}) : super(MoviesStateLoading()) {
    on<MoviesStateChanged>(_moviesStateChanged);
  }

  final MoviesUsecases moviesUsecases;
  // Initialize the bloc
  Future<void> init() async {
    try {
      add(MoviesStateChanged(MoviesState.loading()));
      final List<TrendingMovie> trendingMovies = await _getTrendingMovies();

      final List<Movie> popularMovies = await _getPopularMovies();
      add(MoviesStateChanged(MoviesState.loaded(
          popularMovies: popularMovies, trendingMovies: trendingMovies)));
    } catch (e) {
      add(MoviesStateChanged(MoviesStateFailed()));
    }
  }

  /// The method return a list of popular movies
  Future<List<Movie>> _getPopularMovies({int page = 1}) async {
    final response = await moviesUsecases.getPopular(page: page);
    return response.when(left: (failure) {
      throw (texts.messages.somethingWentWrongContactAdministrator);
    }, right: (popularMovies) {
      return popularMovies;
    });
  }

  /// The method return a list of trending movies
  Future<List<TrendingMovie>> _getTrendingMovies({int page = 1}) async {
    final response = await moviesUsecases.getTendring(page: page);
    return response.when(left: (failure) {
      throw (texts.messages.somethingWentWrongContactAdministrator);
    }, right: (trendingMovies) {
      //
      return trendingMovies;
    });
  }

// change state
  void _moviesStateChanged(
          MoviesStateChanged event, Emitter<MoviesState> emit) =>
      emit(event.state);
}
