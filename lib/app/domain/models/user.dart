import 'dart:convert';

class User {
  User({
    this.photoUrl,
    required this.name,
    required this.email,
  });

  factory User.fromRawJson(String str) => User.fromJson(json.decode(str));

  factory User.fromJson(Map<String, dynamic> json) => User(
        photoUrl: json['photoUrl'],
        name: json['name'],
        email: json['email'],
      );
  String? photoUrl;
  String name;
  String email;

  User copyWith({
    String? photoUrl,
    String? name,
    String? email,
  }) =>
      User(
        photoUrl: photoUrl ?? this.photoUrl,
        name: name ?? this.name,
        email: email ?? this.email,
      );

  String toRawJson() => json.encode(toJson());

  Map<String, dynamic> toJson() => {
        'photoUrl': photoUrl,
        'name': name,
        'email': email,
      };
}
