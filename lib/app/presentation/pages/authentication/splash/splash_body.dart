import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../config/constants/assets.dart';
import '../../../../config/routes/app_routes.dart';
import '../../../blocs/cubit/session_cubit.dart';
import '../../pages.dart';

enum TypeLogin { home, login, error }

class SplashBody extends StatefulWidget {
  const SplashBody({super.key});

  @override
  State<SplashBody> createState() => _SplashBodyState();
}

class _SplashBodyState extends State<SplashBody> {
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) => _init());
  }

  void _init() async {
    final TypeLogin goRoute = await _validateSession();
    switch (goRoute) {
      case TypeLogin.login:
        _goToRoute(LoginPage.route);
        return;
      case TypeLogin.home:
        _goToRoute(HomePage.route);
        return;
      default:
        _goToRoute(LoginPage.route);
    }
  }

  /// The method checks if it is authenticated and returns a typelogin
  Future<TypeLogin> _validateSession() async {
    try {
      final sessionCubit = context.read<SessionCubit>();
      final bool isAuthenticated = await sessionCubit.isAuthenticated();
      if (!isAuthenticated) return TypeLogin.login;
      return TypeLogin.home;
    } catch (e) {
      return TypeLogin.error;
    }
  }

  // route
  void _goToRoute(String route) {
    appRouter.pushReplacement(route);
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Hero(
          tag: Assets.icon,
          child: Image.asset(Assets.icon,
              fit: BoxFit.cover, width: 250, height: 250)),
    );
  }
}
