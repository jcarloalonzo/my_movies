import 'package:flutter_dotenv/flutter_dotenv.dart';

class Environment {
  Environment._();
  static init() async {
    await dotenv.load(fileName: '.env');
  }

  static final String token =
      dotenv.env['TOKEN_USER_TRAKT'] ?? 'Doesnt work url';

  static final String urlService =
      dotenv.env['URL_SERVICE'] ?? 'Doesnt work url';
}
