import '../../models/user.dart';
import '../../repositories/session_repository.dart';

class SessionUsecases {
  SessionUsecases({required this.sessionRepository});

  final SessionRepository sessionRepository;

  Future<User?> signIn() async {
    return sessionRepository.signIn();
  }

  Future<User?> getCurrentUser() {
    return sessionRepository.getCurrentUser();
  }

  Future<void> updateUser(User user) async {
    await sessionRepository.updateUser(user);
  }

  Future<void> signOut() async {
    await sessionRepository.signOut();
  }

  Future<bool> existsToken() {
    return sessionRepository.existsToken();
  }
}
