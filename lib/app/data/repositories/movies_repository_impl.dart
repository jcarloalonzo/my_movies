import '../../domain/datasources/movies_datasource.dart';
import '../../domain/repositories/movies_repository.dart';
import '../either/either.dart';
import '../entities/response/movie_response.dart';
import '../entities/response/trending_response.dart';

class MoviesRepositoryImpl implements MoviesRepository {
  MoviesRepositoryImpl({required this.moviesDatasource});

  final MoviesDatasource moviesDatasource;

  @override
  Future<Either<String, List<Movie>>> getPopular({int page = 1}) {
    return moviesDatasource.getPopular(page: page);
  }

  @override
  Future<Either<String, List<TrendingMovie>>> getTendring({int page = 1}) {
    return moviesDatasource.getTendring(page: page);
  }
}
