import 'package:flutter/material.dart';

import '../../../config/constants/palette.dart';

class TexTitle extends StatelessWidget {
  const TexTitle(
    this.text, {
    super.key,
    this.color = Palette.primaryColor,
    this.textAlign = TextAlign.start,
    this.fontWeight = FontWeight.w700,
    this.fontSize = 20,
  });
  final String text;
  final Color color;
  final TextAlign textAlign;
  final FontWeight fontWeight;
  final double fontSize;
  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;
    return Text(
      text,
      style: textTheme.titleMedium?.copyWith(
        color: color,
        fontWeight: fontWeight,
        fontSize: fontSize,
      ),
      textAlign: textAlign,
    );
  }
}
