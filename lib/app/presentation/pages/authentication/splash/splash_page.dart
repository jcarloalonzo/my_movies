import 'package:flutter/material.dart';

import '../../../../config/constants/palette.dart';
import 'splash_body.dart';

class SplashPage extends StatelessWidget {
  const SplashPage({super.key});
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false,
      child: const Scaffold(
        backgroundColor: Palette.primaryColor,
        body: SplashBody(),
      ),
    );
  }
}
