import 'package:dio/dio.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:get_it/get_it.dart';

import 'app/config/constants/environment.dart';
import 'app/data/datasources/google_datasource.dart';
import 'app/data/datasources/tracktv_datasource.dart';
import 'app/data/http/http.dart';
import 'app/data/repositories/movies_repository_impl.dart';
import 'app/data/repositories/session_repository_impl.dart';
import 'app/data/services/local/language_service.dart';
import 'app/data/services/local/secure_storage_service.dart';
import 'app/data/services/remote/google_signin_service.dart';
import 'app/data/services/remote/tracktv_api.dart';
import 'app/domain/datasources/movies_datasource.dart';
import 'app/domain/datasources/session_datasource.dart';
import 'app/domain/repositories/movies_repository.dart';
import 'app/domain/repositories/session_repository.dart';
import 'app/domain/usecases/movies/movies_usecases.dart';
import 'app/domain/usecases/session/session_usecases.dart';
import 'app/presentation/blocs/cubit/session_cubit.dart';
import 'app/presentation/blocs/movies/movies_bloc.dart';
import 'generated/translations.g.dart';

final getIt = GetIt.instance;

void init() {
  //
  // ! blocs

  getIt.registerFactory(() => MoviesBloc(
        moviesUsecases: getIt.call<MoviesUsecases>(),
      ));

  getIt.registerFactory(() => SessionCubit(
        sessionUsecases: getIt.call<SessionUsecases>(),
      ));
  //
  //
  //

  // ! UseCase
  // login
  getIt.registerLazySingleton(
      () => MoviesUsecases(moviesRepository: getIt.call<MoviesRepository>()));
  getIt.registerLazySingleton(() =>
      SessionUsecases(sessionRepository: getIt.call<SessionRepository>()));

  //
  // ! Repository

  getIt.registerLazySingleton<MoviesRepository>(() =>
      MoviesRepositoryImpl(moviesDatasource: getIt.call<MoviesDatasource>()));

  getIt.registerLazySingleton<SessionRepository>(() => SessionRepositoryImpl(
      sessionDatasource: getIt.call<SessionDatasource>()));
  //
  // ! Datasource
  //
  getIt.registerLazySingleton<MoviesDatasource>(
    () => TrackTvDatasourceImpl(api: getIt.call<TrackTvAPI>()),
  );

  getIt.registerLazySingleton<SessionDatasource>(
    () => GoogleDatasourceImpl(
        googleSignInService: getIt.call<GoogleSignInService>()),
  );

  //
  // ! Externals
  // Secure storage
  getIt.registerLazySingleton<SecureStorageService>(
      () => SecureStorageService(const FlutterSecureStorage()));

  final languageService =
      LanguageService(LocaleSettings.currentLocale.languageCode);
  final dio = Dio(
    BaseOptions(
      baseUrl: Environment.urlService,
      connectTimeout: const Duration(seconds: 38),
      receiveTimeout: const Duration(seconds: 38),
      headers: {
        'trakt-api-version': '2',
        'trakt-api-key': Environment.token,
      },
    ),
  );
  final http = Http(dio);
  //
  getIt.registerLazySingleton<LanguageService>(() => languageService);
  getIt.registerLazySingleton<GoogleSignInService>(
      () => GoogleSignInService(getIt.call<SecureStorageService>()));
  getIt.registerLazySingleton<Http>(() => http);

  // ! APIs
  getIt.registerLazySingleton<TrackTvAPI>(
      () => TrackTvAPI(http: getIt.call<Http>()));
}
