import '../models/user.dart';

abstract class SessionRepository {
  Future<User?> signIn();
  Future<User?> getCurrentUser();
  Future<void> updateUser(User user);
  Future<void> signOut();
  Future<bool> existsToken();
}
