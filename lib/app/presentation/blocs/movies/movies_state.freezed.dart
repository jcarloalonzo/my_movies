// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'movies_state.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$MoviesState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() loading,
    required TResult Function(String? failure) failed,
    required TResult Function(
            List<Movie> popularMovies, List<TrendingMovie> trendingMovies)
        loaded,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? loading,
    TResult? Function(String? failure)? failed,
    TResult? Function(
            List<Movie> popularMovies, List<TrendingMovie> trendingMovies)?
        loaded,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? loading,
    TResult Function(String? failure)? failed,
    TResult Function(
            List<Movie> popularMovies, List<TrendingMovie> trendingMovies)?
        loaded,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(MoviesStateLoading value) loading,
    required TResult Function(MoviesStateFailed value) failed,
    required TResult Function(MoviesStateLoaded value) loaded,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(MoviesStateLoading value)? loading,
    TResult? Function(MoviesStateFailed value)? failed,
    TResult? Function(MoviesStateLoaded value)? loaded,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(MoviesStateLoading value)? loading,
    TResult Function(MoviesStateFailed value)? failed,
    TResult Function(MoviesStateLoaded value)? loaded,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $MoviesStateCopyWith<$Res> {
  factory $MoviesStateCopyWith(
          MoviesState value, $Res Function(MoviesState) then) =
      _$MoviesStateCopyWithImpl<$Res, MoviesState>;
}

/// @nodoc
class _$MoviesStateCopyWithImpl<$Res, $Val extends MoviesState>
    implements $MoviesStateCopyWith<$Res> {
  _$MoviesStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$MoviesStateLoadingImplCopyWith<$Res> {
  factory _$$MoviesStateLoadingImplCopyWith(_$MoviesStateLoadingImpl value,
          $Res Function(_$MoviesStateLoadingImpl) then) =
      __$$MoviesStateLoadingImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$MoviesStateLoadingImplCopyWithImpl<$Res>
    extends _$MoviesStateCopyWithImpl<$Res, _$MoviesStateLoadingImpl>
    implements _$$MoviesStateLoadingImplCopyWith<$Res> {
  __$$MoviesStateLoadingImplCopyWithImpl(_$MoviesStateLoadingImpl _value,
      $Res Function(_$MoviesStateLoadingImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$MoviesStateLoadingImpl implements MoviesStateLoading {
  _$MoviesStateLoadingImpl();

  @override
  String toString() {
    return 'MoviesState.loading()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$MoviesStateLoadingImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() loading,
    required TResult Function(String? failure) failed,
    required TResult Function(
            List<Movie> popularMovies, List<TrendingMovie> trendingMovies)
        loaded,
  }) {
    return loading();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? loading,
    TResult? Function(String? failure)? failed,
    TResult? Function(
            List<Movie> popularMovies, List<TrendingMovie> trendingMovies)?
        loaded,
  }) {
    return loading?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? loading,
    TResult Function(String? failure)? failed,
    TResult Function(
            List<Movie> popularMovies, List<TrendingMovie> trendingMovies)?
        loaded,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(MoviesStateLoading value) loading,
    required TResult Function(MoviesStateFailed value) failed,
    required TResult Function(MoviesStateLoaded value) loaded,
  }) {
    return loading(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(MoviesStateLoading value)? loading,
    TResult? Function(MoviesStateFailed value)? failed,
    TResult? Function(MoviesStateLoaded value)? loaded,
  }) {
    return loading?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(MoviesStateLoading value)? loading,
    TResult Function(MoviesStateFailed value)? failed,
    TResult Function(MoviesStateLoaded value)? loaded,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading(this);
    }
    return orElse();
  }
}

abstract class MoviesStateLoading implements MoviesState {
  factory MoviesStateLoading() = _$MoviesStateLoadingImpl;
}

/// @nodoc
abstract class _$$MoviesStateFailedImplCopyWith<$Res> {
  factory _$$MoviesStateFailedImplCopyWith(_$MoviesStateFailedImpl value,
          $Res Function(_$MoviesStateFailedImpl) then) =
      __$$MoviesStateFailedImplCopyWithImpl<$Res>;
  @useResult
  $Res call({String? failure});
}

/// @nodoc
class __$$MoviesStateFailedImplCopyWithImpl<$Res>
    extends _$MoviesStateCopyWithImpl<$Res, _$MoviesStateFailedImpl>
    implements _$$MoviesStateFailedImplCopyWith<$Res> {
  __$$MoviesStateFailedImplCopyWithImpl(_$MoviesStateFailedImpl _value,
      $Res Function(_$MoviesStateFailedImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? failure = freezed,
  }) {
    return _then(_$MoviesStateFailedImpl(
      freezed == failure
          ? _value.failure
          : failure // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc

class _$MoviesStateFailedImpl implements MoviesStateFailed {
  _$MoviesStateFailedImpl([this.failure]);

  @override
  final String? failure;

  @override
  String toString() {
    return 'MoviesState.failed(failure: $failure)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$MoviesStateFailedImpl &&
            (identical(other.failure, failure) || other.failure == failure));
  }

  @override
  int get hashCode => Object.hash(runtimeType, failure);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$MoviesStateFailedImplCopyWith<_$MoviesStateFailedImpl> get copyWith =>
      __$$MoviesStateFailedImplCopyWithImpl<_$MoviesStateFailedImpl>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() loading,
    required TResult Function(String? failure) failed,
    required TResult Function(
            List<Movie> popularMovies, List<TrendingMovie> trendingMovies)
        loaded,
  }) {
    return failed(failure);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? loading,
    TResult? Function(String? failure)? failed,
    TResult? Function(
            List<Movie> popularMovies, List<TrendingMovie> trendingMovies)?
        loaded,
  }) {
    return failed?.call(failure);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? loading,
    TResult Function(String? failure)? failed,
    TResult Function(
            List<Movie> popularMovies, List<TrendingMovie> trendingMovies)?
        loaded,
    required TResult orElse(),
  }) {
    if (failed != null) {
      return failed(failure);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(MoviesStateLoading value) loading,
    required TResult Function(MoviesStateFailed value) failed,
    required TResult Function(MoviesStateLoaded value) loaded,
  }) {
    return failed(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(MoviesStateLoading value)? loading,
    TResult? Function(MoviesStateFailed value)? failed,
    TResult? Function(MoviesStateLoaded value)? loaded,
  }) {
    return failed?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(MoviesStateLoading value)? loading,
    TResult Function(MoviesStateFailed value)? failed,
    TResult Function(MoviesStateLoaded value)? loaded,
    required TResult orElse(),
  }) {
    if (failed != null) {
      return failed(this);
    }
    return orElse();
  }
}

abstract class MoviesStateFailed implements MoviesState {
  factory MoviesStateFailed([final String? failure]) = _$MoviesStateFailedImpl;

  String? get failure;
  @JsonKey(ignore: true)
  _$$MoviesStateFailedImplCopyWith<_$MoviesStateFailedImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$MoviesStateLoadedImplCopyWith<$Res> {
  factory _$$MoviesStateLoadedImplCopyWith(_$MoviesStateLoadedImpl value,
          $Res Function(_$MoviesStateLoadedImpl) then) =
      __$$MoviesStateLoadedImplCopyWithImpl<$Res>;
  @useResult
  $Res call({List<Movie> popularMovies, List<TrendingMovie> trendingMovies});
}

/// @nodoc
class __$$MoviesStateLoadedImplCopyWithImpl<$Res>
    extends _$MoviesStateCopyWithImpl<$Res, _$MoviesStateLoadedImpl>
    implements _$$MoviesStateLoadedImplCopyWith<$Res> {
  __$$MoviesStateLoadedImplCopyWithImpl(_$MoviesStateLoadedImpl _value,
      $Res Function(_$MoviesStateLoadedImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? popularMovies = null,
    Object? trendingMovies = null,
  }) {
    return _then(_$MoviesStateLoadedImpl(
      popularMovies: null == popularMovies
          ? _value._popularMovies
          : popularMovies // ignore: cast_nullable_to_non_nullable
              as List<Movie>,
      trendingMovies: null == trendingMovies
          ? _value._trendingMovies
          : trendingMovies // ignore: cast_nullable_to_non_nullable
              as List<TrendingMovie>,
    ));
  }
}

/// @nodoc

class _$MoviesStateLoadedImpl implements MoviesStateLoaded {
  _$MoviesStateLoadedImpl(
      {required final List<Movie> popularMovies,
      required final List<TrendingMovie> trendingMovies})
      : _popularMovies = popularMovies,
        _trendingMovies = trendingMovies;

  final List<Movie> _popularMovies;
  @override
  List<Movie> get popularMovies {
    if (_popularMovies is EqualUnmodifiableListView) return _popularMovies;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_popularMovies);
  }

  final List<TrendingMovie> _trendingMovies;
  @override
  List<TrendingMovie> get trendingMovies {
    if (_trendingMovies is EqualUnmodifiableListView) return _trendingMovies;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_trendingMovies);
  }

  @override
  String toString() {
    return 'MoviesState.loaded(popularMovies: $popularMovies, trendingMovies: $trendingMovies)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$MoviesStateLoadedImpl &&
            const DeepCollectionEquality()
                .equals(other._popularMovies, _popularMovies) &&
            const DeepCollectionEquality()
                .equals(other._trendingMovies, _trendingMovies));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(_popularMovies),
      const DeepCollectionEquality().hash(_trendingMovies));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$MoviesStateLoadedImplCopyWith<_$MoviesStateLoadedImpl> get copyWith =>
      __$$MoviesStateLoadedImplCopyWithImpl<_$MoviesStateLoadedImpl>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() loading,
    required TResult Function(String? failure) failed,
    required TResult Function(
            List<Movie> popularMovies, List<TrendingMovie> trendingMovies)
        loaded,
  }) {
    return loaded(popularMovies, trendingMovies);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? loading,
    TResult? Function(String? failure)? failed,
    TResult? Function(
            List<Movie> popularMovies, List<TrendingMovie> trendingMovies)?
        loaded,
  }) {
    return loaded?.call(popularMovies, trendingMovies);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? loading,
    TResult Function(String? failure)? failed,
    TResult Function(
            List<Movie> popularMovies, List<TrendingMovie> trendingMovies)?
        loaded,
    required TResult orElse(),
  }) {
    if (loaded != null) {
      return loaded(popularMovies, trendingMovies);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(MoviesStateLoading value) loading,
    required TResult Function(MoviesStateFailed value) failed,
    required TResult Function(MoviesStateLoaded value) loaded,
  }) {
    return loaded(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(MoviesStateLoading value)? loading,
    TResult? Function(MoviesStateFailed value)? failed,
    TResult? Function(MoviesStateLoaded value)? loaded,
  }) {
    return loaded?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(MoviesStateLoading value)? loading,
    TResult Function(MoviesStateFailed value)? failed,
    TResult Function(MoviesStateLoaded value)? loaded,
    required TResult orElse(),
  }) {
    if (loaded != null) {
      return loaded(this);
    }
    return orElse();
  }
}

abstract class MoviesStateLoaded implements MoviesState {
  factory MoviesStateLoaded(
          {required final List<Movie> popularMovies,
          required final List<TrendingMovie> trendingMovies}) =
      _$MoviesStateLoadedImpl;

  List<Movie> get popularMovies;
  List<TrendingMovie> get trendingMovies;
  @JsonKey(ignore: true)
  _$$MoviesStateLoadedImplCopyWith<_$MoviesStateLoadedImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
