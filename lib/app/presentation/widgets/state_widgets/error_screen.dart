import 'package:flutter/material.dart';

import '../../../../../generated/translations.g.dart';
import '../../../config/constants/palette.dart';
import '../texts/text_body.dart';

class ErrorScreen extends StatelessWidget {
  const ErrorScreen(this.text, {super.key, required this.onRetry});
  final VoidCallback onRetry;
  final String? text;
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          TextBody(
            text ?? texts.messages.somethingWentWrongContactAdministrator,
            textAlign: TextAlign.center,
            // 'hola',
            fontWeight: FontWeight.w500,
          ),
          const SizedBox(height: 10),
          MaterialButton(
            onPressed: onRetry,
            color: Palette.primaryColor,
            child: TextBody(
              texts.label.retry,
              color: Colors.white,
              fontWeight: FontWeight.w500,
            ),
          ),
          const SizedBox(height: 20),
        ],
      ),
    );
  }
}
