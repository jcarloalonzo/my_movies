import 'dart:convert';

import 'package:firebase_database/firebase_database.dart';
import 'package:google_sign_in/google_sign_in.dart';

import '../../../domain/models/user.dart';
import '../../mappers/user_mappers.dart';
import '../local/secure_storage_service.dart';

const String _tableDatabase = 'users';

class GoogleSignInService {
  GoogleSignInService(this._localStorage);

  final SecureStorageService _localStorage;

  static final GoogleSignIn _googleSignIn = GoogleSignIn(
    scopes: [
      'email',
    ],
  );

  /// The method returns the User from google
  Future<User?> signIn() async {
    try {
      final GoogleSignInAccount? account = await _googleSignIn.signIn();
      if (account == null) return null;
      // UserMapper - use to convert the Google object to User.
      User user = UserMappers.googleToUser(account);
      DatabaseReference databaseReference =
          FirebaseDatabase.instance.ref().child(_tableDatabase);
      // save in database
      databaseReference.child(account.id).set(user.toJson());

      // Save the token user id in local storage
      _localStorage.saveIdUser(account.id);
      return user;
    } catch (e) {
      return null;
    }
  }

  /// Get the current user with the active session in the app
  Future<User?> getCurrentUser() async {
    try {
      DatabaseReference databaseReference =
          FirebaseDatabase.instance.ref().child(_tableDatabase);
      final String? idUser = await _localStorage.idUser;
      if (idUser == null) throw 'No data in the secure storage';
      DataSnapshot data = await databaseReference.child(idUser).get();
      if (data.value == null) throw 'The user does not exist in the database';
      // convert userData to JSON
      final String userDataJson = jsonEncode(data.value);
      // convert the string JSON to map (Map)
      final userDataMap = jsonDecode(userDataJson);
      final User user = User.fromJson(userDataMap);
      return user;
    } catch (e) {
      return null;
    }
  }

  /// Update the user in database
  Future<void> updateUser(User user) async {
    final String? idUser = await _localStorage.idUser;
    if (idUser == null) throw 'No data in the secure storage';
    DatabaseReference databaseReference =
        FirebaseDatabase.instance.ref().child(_tableDatabase);
    databaseReference.child(idUser).set(user.toJson());
  }

  void _createRecord(User user) async {
    final String? idUser = await _localStorage.idUser;
    if (idUser == null) throw 'No data in the secure storage';
    DatabaseReference databaseReference =
        FirebaseDatabase.instance.ref().child(_tableDatabase);
    databaseReference.child(idUser).set(user.toJson());
  }

  Future<void> signOut() async {
    // signOut in google services
    await _googleSignIn.signOut();
    // signOut in localStorage
    await _localStorage.signOut();
  }

  /// The methods returns true if exists idUser ( Token ) in the local storage
  Future<bool> existsToken() async {
    final String? idUser = await _localStorage.idUser;
    return idUser != null;
  }
}
