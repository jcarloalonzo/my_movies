import '../../../data/either/either.dart';
import '../../../data/entities/response/movie_response.dart';
import '../../../data/entities/response/trending_response.dart';
import '../../repositories/movies_repository.dart';

class MoviesUsecases {
  MoviesUsecases({required this.moviesRepository});
  final MoviesRepository moviesRepository;

  Future<Either<String, List<TrendingMovie>>> getTendring({int page = 1}) {
    return moviesRepository.getTendring(page: page);
  }

  Future<Either<String, List<Movie>>> getPopular({int page = 1}) {
    return moviesRepository.getPopular(page: page);
  }
}
