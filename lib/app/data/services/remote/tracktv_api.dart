import '../../either/either.dart';
import '../../entities/response/movie_response.dart';
import '../../entities/response/trending_response.dart';
import '../../http/http.dart';

class TrackTvAPI {
  TrackTvAPI({required this.http});

  final Http http;

  Future<Either<String, List<TrendingMovie>>> getTrending(int page) async {
    final response = await http.request(
      '/movies/trending?page=$page',
      onSuccess: (json) {
        final list = json as List;
        return list.map((e) => TrendingMovie.fromJson(e)).toList();
      },
    );
    return response.when(
      left: (failure) => Either.left(failure),
      right: (value) => Either.right(value),
    );
  }

  Future<Either<String, List<Movie>>> getPopular(int page) async {
    try {
      final response = await http.request(
        '/movies/popular?page=$page',
        onSuccess: (json) {
          final list = json as List;
          return list.map((e) => Movie.fromJson(e)).toList();
        },
      );
      return response.when(
        left: (failure) => Either.left(failure),
        right: (value) => Either.right(value),
      );
    } catch (e) {
      return Either.left(e.toString());
    }
  }
}
