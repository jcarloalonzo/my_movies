import 'dart:convert';

class Movie {
  Movie({
    required this.title,
    required this.year,
    required this.ids,
  });

  factory Movie.fromRawJson(String str) => Movie.fromJson(json.decode(str));

  factory Movie.fromJson(Map<String, dynamic> json) => Movie(
        title: json['title'],
        year: json['year'],
        ids: Ids.fromJson(json['ids']),
      );
  String title;
  int year;
  Ids ids;

  String toRawJson() => json.encode(toJson());

  Map<String, dynamic> toJson() => {
        'title': title,
        'year': year,
        'ids': ids.toJson(),
      };
}

class Ids {
  Ids({
    this.trakt,
    this.slug,
    this.tvdb,
    this.imdb,
    this.tmdb,
    this.tvrage,
  });

  factory Ids.fromRawJson(String str) => Ids.fromJson(json.decode(str));

  factory Ids.fromJson(Map<String, dynamic> json) => Ids(
        trakt: json['trakt'],
        slug: json['slug'],
        tvdb: json['tvdb'],
        imdb: json['imdb'],
        tmdb: json['tmdb'],
        tvrage: json['tvrage'],
      );
  int? trakt;
  String? slug;
  int? tvdb;
  String? imdb;
  int? tmdb;
  int? tvrage;

  String toRawJson() => json.encode(toJson());

  Map<String, dynamic> toJson() => {
        'trakt': trakt,
        'slug': slug,
        'tvdb': tvdb,
        'imdb': imdb,
        'tmdb': tmdb,
        'tvrage': tvrage,
      };
}
