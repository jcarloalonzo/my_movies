import '../../data/either/either.dart';
import '../../data/entities/response/movie_response.dart';
import '../../data/entities/response/trending_response.dart';

abstract class MoviesRepository {
  //
  //
  Future<Either<String, List<TrendingMovie>>> getTendring({int page = 1});
  Future<Either<String, List<Movie>>> getPopular({int page = 1});
}
