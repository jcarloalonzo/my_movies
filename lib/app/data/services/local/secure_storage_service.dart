import 'package:flutter_secure_storage/flutter_secure_storage.dart';

const String _idUser = 'ID_USER';

class SecureStorageService {
  SecureStorageService(this._secureStorage);

  final FlutterSecureStorage _secureStorage;

  Future<String?> get idUser async {
    final idUser = await _secureStorage.read(key: _idUser);
    return idUser;
  }

  Future<void> saveIdUser(String idUser) async {
    return _secureStorage.write(key: _idUser, value: idUser);
  }

  Future<void> signOut() => _secureStorage.deleteAll();
}
