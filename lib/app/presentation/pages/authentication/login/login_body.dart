import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';

import '../../../../../generated/translations.g.dart';
import '../../../../config/constants/assets.dart';
import '../../../../config/constants/palette.dart';
import '../../../blocs/cubit/session_cubit.dart';
import '../../home/home_page.dart';

class LoginBody extends StatelessWidget {
  const LoginBody({super.key});

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return Column(
      children: <Widget>[
        // Image
        SizedBox(
          width: size.width,
          height: size.height * 0.65,
          child: Hero(
            tag: Assets.icon,
            child: Bounce(
              animate: true,
              infinite: true,
              duration: const Duration(seconds: 4),
              child: Center(
                child: Image.asset(
                  Assets.icon,
                  fit: BoxFit.contain,
                  width: 250,
                  height: 250,
                ),
              ),
            ),
          ),
        ),

        //  Body view
        Expanded(
          child: Container(
            width: size.width,
            decoration: BoxDecoration(
              color: Colors.white,
              border: Border.all(
                color: Colors.white,
                width: 2,
              ),
              borderRadius: const BorderRadius.only(
                  topLeft: Radius.circular(50), topRight: Radius.circular(50)),
            ),
            child: const _Body(),
          ),
        ),
      ],
    );
  }
}

class _Body extends StatelessWidget {
  const _Body();

  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;

    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 40.0, vertical: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            'My Movie',
            style: textTheme.titleLarge?.copyWith(
              color: Palette.primaryColor,
              fontSize: 40,
              fontWeight: FontWeight.w800,
            ),
          ),
          const SizedBox(height: 5),
          Text('${texts.label.welcome}!',
              style: const TextStyle(
                color: Palette.primaryColor,
                fontSize: 20,
                fontWeight: FontWeight.w500,
              )),
          const SizedBox(height: 5),
          RichText(
            text: TextSpan(
              style: DefaultTextStyle.of(context).style,
              children: [
                TextSpan(
                  text: texts.label.enterTheApplicationWithYour,
                  style: const TextStyle(
                    color: Colors.black54,
                    fontWeight: FontWeight.w500,
                    fontFamily: 'Poppins',
                  ),
                ),
                const TextSpan(
                  text: ' Gmail.',
                  style: TextStyle(
                    color: Colors.red,
                    fontWeight: FontWeight.w800,
                    fontFamily: 'Poppins',
                  ),
                ),
              ],
            ),
          ),
          const Spacer(),
          SizedBox(
            child: FilledButton(
                style: ButtonStyle(
                    backgroundColor: MaterialStateProperty.all(Colors.red),
                    shape: MaterialStateProperty.all(RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(14)))),
                onPressed: () => _onTapGmail(context),
                child: const Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      'G',
                      style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.w700,
                          fontFamily: 'Poppins',
                          fontSize: 25),
                    ),
                    SizedBox(width: 10),
                    Text('Gmail'),
                  ],
                )),
          ),
          const SizedBox(height: 10),
        ],
      ),
    );
  }

  void _onTapGmail(BuildContext context) async {
    final sessionCubit = context.read<SessionCubit>();
    final bool validate = await sessionCubit.signInGoogle();
    if (!validate) return;
    if (context.mounted) context.push(HomePage.route);
  }
}
