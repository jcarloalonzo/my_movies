/// Generated file. Do not edit.
///
/// Original: i18n
/// To regenerate, run: `dart run slang`
///
/// Locales: 2
/// Strings: 64 (32 per locale)
///
/// Built on 2023-11-12 at 23:32 UTC

// coverage:ignore-file
// ignore_for_file: type=lint

import 'package:flutter/widgets.dart';
import 'package:slang/builder/model/node.dart';
import 'package:slang_flutter/slang_flutter.dart';
export 'package:slang_flutter/slang_flutter.dart';

const AppLocale _baseLocale = AppLocale.en;

/// Supported locales, see extension methods below.
///
/// Usage:
/// - LocaleSettings.setLocale(AppLocale.en) // set locale
/// - Locale locale = AppLocale.en.flutterLocale // get flutter locale from enum
/// - if (LocaleSettings.currentLocale == AppLocale.en) // locale check
enum AppLocale with BaseAppLocale<AppLocale, _TranslationsEn> {
	en(languageCode: 'en', build: _TranslationsEn.build),
	es(languageCode: 'es', build: _TranslationsEs.build);

	const AppLocale({required this.languageCode, this.scriptCode, this.countryCode, required this.build}); // ignore: unused_element

	@override final String languageCode;
	@override final String? scriptCode;
	@override final String? countryCode;
	@override final TranslationBuilder<AppLocale, _TranslationsEn> build;

	/// Gets current instance managed by [LocaleSettings].
	_TranslationsEn get translations => LocaleSettings.instance.translationMap[this]!;
}

/// Method A: Simple
///
/// No rebuild after locale change.
/// Translation happens during initialization of the widget (call of texts).
/// Configurable via 'translate_var'.
///
/// Usage:
/// String a = texts.someKey.anotherKey;
/// String b = texts['someKey.anotherKey']; // Only for edge cases!
_TranslationsEn get texts => LocaleSettings.instance.currentTranslations;

/// Method B: Advanced
///
/// All widgets using this method will trigger a rebuild when locale changes.
/// Use this if you have e.g. a settings page where the user can select the locale during runtime.
///
/// Step 1:
/// wrap your App with
/// TranslationProvider(
/// 	child: MyApp()
/// );
///
/// Step 2:
/// final texts = Translations.of(context); // Get texts variable.
/// String a = texts.someKey.anotherKey; // Use texts variable.
/// String b = texts['someKey.anotherKey']; // Only for edge cases!
class Translations {
	Translations._(); // no constructor

	static _TranslationsEn of(BuildContext context) => InheritedLocaleData.of<AppLocale, _TranslationsEn>(context).translations;
}

/// The provider for method B
class TranslationProvider extends BaseTranslationProvider<AppLocale, _TranslationsEn> {
	TranslationProvider({required super.child}) : super(settings: LocaleSettings.instance);

	static InheritedLocaleData<AppLocale, _TranslationsEn> of(BuildContext context) => InheritedLocaleData.of<AppLocale, _TranslationsEn>(context);
}

/// Method B shorthand via [BuildContext] extension method.
/// Configurable via 'translate_var'.
///
/// Usage (e.g. in a widget's build method):
/// context.texts.someKey.anotherKey
extension BuildContextTranslationsExtension on BuildContext {
	_TranslationsEn get texts => TranslationProvider.of(this).translations;
}

/// Manages all translation instances and the current locale
class LocaleSettings extends BaseFlutterLocaleSettings<AppLocale, _TranslationsEn> {
	LocaleSettings._() : super(utils: AppLocaleUtils.instance);

	static final instance = LocaleSettings._();

	// static aliases (checkout base methods for documentation)
	static AppLocale get currentLocale => instance.currentLocale;
	static Stream<AppLocale> getLocaleStream() => instance.getLocaleStream();
	static AppLocale setLocale(AppLocale locale, {bool? listenToDeviceLocale = false}) => instance.setLocale(locale, listenToDeviceLocale: listenToDeviceLocale);
	static AppLocale setLocaleRaw(String rawLocale, {bool? listenToDeviceLocale = false}) => instance.setLocaleRaw(rawLocale, listenToDeviceLocale: listenToDeviceLocale);
	static AppLocale useDeviceLocale() => instance.useDeviceLocale();
	@Deprecated('Use [AppLocaleUtils.supportedLocales]') static List<Locale> get supportedLocales => instance.supportedLocales;
	@Deprecated('Use [AppLocaleUtils.supportedLocalesRaw]') static List<String> get supportedLocalesRaw => instance.supportedLocalesRaw;
	static void setPluralResolver({String? language, AppLocale? locale, PluralResolver? cardinalResolver, PluralResolver? ordinalResolver}) => instance.setPluralResolver(
		language: language,
		locale: locale,
		cardinalResolver: cardinalResolver,
		ordinalResolver: ordinalResolver,
	);
}

/// Provides utility functions without any side effects.
class AppLocaleUtils extends BaseAppLocaleUtils<AppLocale, _TranslationsEn> {
	AppLocaleUtils._() : super(baseLocale: _baseLocale, locales: AppLocale.values);

	static final instance = AppLocaleUtils._();

	// static aliases (checkout base methods for documentation)
	static AppLocale parse(String rawLocale) => instance.parse(rawLocale);
	static AppLocale parseLocaleParts({required String languageCode, String? scriptCode, String? countryCode}) => instance.parseLocaleParts(languageCode: languageCode, scriptCode: scriptCode, countryCode: countryCode);
	static AppLocale findDeviceLocale() => instance.findDeviceLocale();
	static List<Locale> get supportedLocales => instance.supportedLocales;
	static List<String> get supportedLocalesRaw => instance.supportedLocalesRaw;
}

// translations

// Path: <root>
class _TranslationsEn implements BaseTranslations<AppLocale, _TranslationsEn> {

	/// You can call this constructor and build your own translation instance of this locale.
	/// Constructing via the enum [AppLocale.build] is preferred.
	_TranslationsEn.build({Map<String, Node>? overrides, PluralResolver? cardinalResolver, PluralResolver? ordinalResolver})
		: assert(overrides == null, 'Set "translation_overrides: true" in order to enable this feature.'),
		  $meta = TranslationMetadata(
		    locale: AppLocale.en,
		    overrides: overrides ?? {},
		    cardinalResolver: cardinalResolver,
		    ordinalResolver: ordinalResolver,
		  ) {
		$meta.setFlatMapFunction(_flatMapFunction);
	}

	/// Metadata for the translations of <en>.
	@override final TranslationMetadata<AppLocale, _TranslationsEn> $meta;

	/// Access flat map
	dynamic operator[](String key) => $meta.getTranslation(key);

	late final _TranslationsEn _root = this; // ignore: unused_field

	// Translations
	late final _TranslationsLabelEn label = _TranslationsLabelEn._(_root);
	late final _TranslationsMessagesEn messages = _TranslationsMessagesEn._(_root);
	late final _TranslationsResponseEn response = _TranslationsResponseEn._(_root);
}

// Path: label
class _TranslationsLabelEn {
	_TranslationsLabelEn._(this._root);

	final _TranslationsEn _root; // ignore: unused_field

	// Translations
	String get movies => 'Movies';
	String get profile => 'Profile';
	String get welcome => 'Welcome';
	String get enterTheApplicationWithYour => 'Enter the application with your';
	String get retry => 'Retry';
	String get watchingNow => 'Watching now';
	String get trending => 'Trending';
	String get popular => 'Popular';
	String get year => 'Year';
	String get update => 'Update';
}

// Path: messages
class _TranslationsMessagesEn {
	_TranslationsMessagesEn._(this._root);

	final _TranslationsEn _root; // ignore: unused_field

	// Translations
	String get somethingWentWrongContactAdministrator => 'Something went wrong, please contact your administrator';
	String get doYouWantToLogout => 'Do you want to log out?';
}

// Path: response
class _TranslationsResponseEn {
	_TranslationsResponseEn._(this._root);

	final _TranslationsEn _root; // ignore: unused_field

	// Translations
	late final _TranslationsResponseErrorEn error = _TranslationsResponseErrorEn._(_root);
	late final _TranslationsResponseMessageStatusEn messageStatus = _TranslationsResponseMessageStatusEn._(_root);
}

// Path: response.error
class _TranslationsResponseErrorEn {
	_TranslationsResponseErrorEn._(this._root);

	final _TranslationsEn _root; // ignore: unused_field

	// Translations
	String get theRequestToTheApiServerWasCanceled => 'The request to the API server was canceled';
	String get connectionTimeoutWithApiServer => 'Connection timeout with API server';
	String get receiveTimeoutInConnectionWithApiServer => 'Receive timeout relative to API server';
	String get sendTimeoutInConnectionWithApiServer => 'Send timeout in connection with API server';
	String get noInternet => 'No internet';
	String get unexpectedErrorOcurred => 'Unexpected error occurred';
	String get somethingWentWrong => 'Something went wrong';
	String get badRequest => 'Bad Request';
	String get notAuthorized => 'Not authorized';
	String get forbidden => 'Forbidden';
	String get badGateway => 'bad gateway';
}

// Path: response.messageStatus
class _TranslationsResponseMessageStatusEn {
	_TranslationsResponseMessageStatusEn._(this._root);

	final _TranslationsEn _root; // ignore: unused_field

	// Translations
	String get unauthorizedUser => 'Unauthorized user';
	String get resourceNotFound => 'Resource not found';
	String get internalError => 'Internal error';
	String get itemDuplicated => 'Item duplicated';
	String get incorrectCredentials => 'Incorrect credentials';
	String get theSelectedEquipmentIsInactive => 'The selected Equipment is inactive';
	String get theSelectedSubEquipmentIsInactive => 'The selected Sub-Equipment is inactive';
	String get theOfferIsNoLongerAvailable => 'The offer is no longer available';
	String get unidentifiedError => 'Unidentified error';
}

// Path: <root>
class _TranslationsEs implements _TranslationsEn {

	/// You can call this constructor and build your own translation instance of this locale.
	/// Constructing via the enum [AppLocale.build] is preferred.
	_TranslationsEs.build({Map<String, Node>? overrides, PluralResolver? cardinalResolver, PluralResolver? ordinalResolver})
		: assert(overrides == null, 'Set "translation_overrides: true" in order to enable this feature.'),
		  $meta = TranslationMetadata(
		    locale: AppLocale.es,
		    overrides: overrides ?? {},
		    cardinalResolver: cardinalResolver,
		    ordinalResolver: ordinalResolver,
		  ) {
		$meta.setFlatMapFunction(_flatMapFunction);
	}

	/// Metadata for the translations of <es>.
	@override final TranslationMetadata<AppLocale, _TranslationsEn> $meta;

	/// Access flat map
	@override dynamic operator[](String key) => $meta.getTranslation(key);

	@override late final _TranslationsEs _root = this; // ignore: unused_field

	// Translations
	@override late final _TranslationsLabelEs label = _TranslationsLabelEs._(_root);
	@override late final _TranslationsMessagesEs messages = _TranslationsMessagesEs._(_root);
	@override late final _TranslationsResponseEs response = _TranslationsResponseEs._(_root);
}

// Path: label
class _TranslationsLabelEs implements _TranslationsLabelEn {
	_TranslationsLabelEs._(this._root);

	@override final _TranslationsEs _root; // ignore: unused_field

	// Translations
	@override String get movies => 'Películas';
	@override String get profile => 'Perfil';
	@override String get welcome => 'Bienvenido';
	@override String get enterTheApplicationWithYour => 'Ingresa a la aplicación con tu';
	@override String get retry => 'Reintentar';
	@override String get watchingNow => 'Mirando ahora';
	@override String get trending => 'Tendencia';
	@override String get popular => 'Popular';
	@override String get year => 'Año';
	@override String get update => 'Actualizar';
}

// Path: messages
class _TranslationsMessagesEs implements _TranslationsMessagesEn {
	_TranslationsMessagesEs._(this._root);

	@override final _TranslationsEs _root; // ignore: unused_field

	// Translations
	@override String get somethingWentWrongContactAdministrator => 'Algo salió mal, por favor contacte a su administrador';
	@override String get doYouWantToLogout => '¿Quieres cerrar sesión?';
}

// Path: response
class _TranslationsResponseEs implements _TranslationsResponseEn {
	_TranslationsResponseEs._(this._root);

	@override final _TranslationsEs _root; // ignore: unused_field

	// Translations
	@override late final _TranslationsResponseErrorEs error = _TranslationsResponseErrorEs._(_root);
	@override late final _TranslationsResponseMessageStatusEs messageStatus = _TranslationsResponseMessageStatusEs._(_root);
}

// Path: response.error
class _TranslationsResponseErrorEs implements _TranslationsResponseErrorEn {
	_TranslationsResponseErrorEs._(this._root);

	@override final _TranslationsEs _root; // ignore: unused_field

	// Translations
	@override String get theRequestToTheApiServerWasCanceled => 'La solicitud al servidor API fue cancelada';
	@override String get connectionTimeoutWithApiServer => 'Tiempo de espera de conexión del servidor API';
	@override String get receiveTimeoutInConnectionWithApiServer => 'Tiempo de espera de recepción relativo al servidor API';
	@override String get sendTimeoutInConnectionWithApiServer => 'Enviar tiempo de espera en conexión con el servidor API';
	@override String get noInternet => 'Sin Internet';
	@override String get unexpectedErrorOcurred => 'Ocurrió un error inesperado';
	@override String get somethingWentWrong => 'Algo salió mal';
	@override String get badRequest => 'Solicitud incorrecta';
	@override String get notAuthorized => 'No autorizado';
	@override String get forbidden => 'Prohibido';
	@override String get badGateway => 'Puerta de enlace incorrecta';
}

// Path: response.messageStatus
class _TranslationsResponseMessageStatusEs implements _TranslationsResponseMessageStatusEn {
	_TranslationsResponseMessageStatusEs._(this._root);

	@override final _TranslationsEs _root; // ignore: unused_field

	// Translations
	@override String get unauthorizedUser => 'Usuario no autorizado';
	@override String get resourceNotFound => 'Recurso no encontrado';
	@override String get internalError => 'Error interno';
	@override String get itemDuplicated => 'Registro ya existe';
	@override String get incorrectCredentials => 'Credenciales incorrectas';
	@override String get theSelectedEquipmentIsInactive => 'El Equipo seleccionado se encuentra inactivo';
	@override String get theSelectedSubEquipmentIsInactive => 'El Sub-Equipo seleccionado se encuentra inactivo';
	@override String get theOfferIsNoLongerAvailable => 'La oferta ya no está disponible';
	@override String get unidentifiedError => 'Error no identificado';
}

/// Flat map(s) containing all translations.
/// Only for edge cases! For simple maps, use the map function of this library.

extension on _TranslationsEn {
	dynamic _flatMapFunction(String path) {
		switch (path) {
			case 'label.movies': return 'Movies';
			case 'label.profile': return 'Profile';
			case 'label.welcome': return 'Welcome';
			case 'label.enterTheApplicationWithYour': return 'Enter the application with your';
			case 'label.retry': return 'Retry';
			case 'label.watchingNow': return 'Watching now';
			case 'label.trending': return 'Trending';
			case 'label.popular': return 'Popular';
			case 'label.year': return 'Year';
			case 'label.update': return 'Update';
			case 'messages.somethingWentWrongContactAdministrator': return 'Something went wrong, please contact your administrator';
			case 'messages.doYouWantToLogout': return 'Do you want to log out?';
			case 'response.error.theRequestToTheApiServerWasCanceled': return 'The request to the API server was canceled';
			case 'response.error.connectionTimeoutWithApiServer': return 'Connection timeout with API server';
			case 'response.error.receiveTimeoutInConnectionWithApiServer': return 'Receive timeout relative to API server';
			case 'response.error.sendTimeoutInConnectionWithApiServer': return 'Send timeout in connection with API server';
			case 'response.error.noInternet': return 'No internet';
			case 'response.error.unexpectedErrorOcurred': return 'Unexpected error occurred';
			case 'response.error.somethingWentWrong': return 'Something went wrong';
			case 'response.error.badRequest': return 'Bad Request';
			case 'response.error.notAuthorized': return 'Not authorized';
			case 'response.error.forbidden': return 'Forbidden';
			case 'response.error.badGateway': return 'bad gateway';
			case 'response.messageStatus.unauthorizedUser': return 'Unauthorized user';
			case 'response.messageStatus.resourceNotFound': return 'Resource not found';
			case 'response.messageStatus.internalError': return 'Internal error';
			case 'response.messageStatus.itemDuplicated': return 'Item duplicated';
			case 'response.messageStatus.incorrectCredentials': return 'Incorrect credentials';
			case 'response.messageStatus.theSelectedEquipmentIsInactive': return 'The selected Equipment is inactive';
			case 'response.messageStatus.theSelectedSubEquipmentIsInactive': return 'The selected Sub-Equipment is inactive';
			case 'response.messageStatus.theOfferIsNoLongerAvailable': return 'The offer is no longer available';
			case 'response.messageStatus.unidentifiedError': return 'Unidentified error';
			default: return null;
		}
	}
}

extension on _TranslationsEs {
	dynamic _flatMapFunction(String path) {
		switch (path) {
			case 'label.movies': return 'Películas';
			case 'label.profile': return 'Perfil';
			case 'label.welcome': return 'Bienvenido';
			case 'label.enterTheApplicationWithYour': return 'Ingresa a la aplicación con tu';
			case 'label.retry': return 'Reintentar';
			case 'label.watchingNow': return 'Mirando ahora';
			case 'label.trending': return 'Tendencia';
			case 'label.popular': return 'Popular';
			case 'label.year': return 'Año';
			case 'label.update': return 'Actualizar';
			case 'messages.somethingWentWrongContactAdministrator': return 'Algo salió mal, por favor contacte a su administrador';
			case 'messages.doYouWantToLogout': return '¿Quieres cerrar sesión?';
			case 'response.error.theRequestToTheApiServerWasCanceled': return 'La solicitud al servidor API fue cancelada';
			case 'response.error.connectionTimeoutWithApiServer': return 'Tiempo de espera de conexión del servidor API';
			case 'response.error.receiveTimeoutInConnectionWithApiServer': return 'Tiempo de espera de recepción relativo al servidor API';
			case 'response.error.sendTimeoutInConnectionWithApiServer': return 'Enviar tiempo de espera en conexión con el servidor API';
			case 'response.error.noInternet': return 'Sin Internet';
			case 'response.error.unexpectedErrorOcurred': return 'Ocurrió un error inesperado';
			case 'response.error.somethingWentWrong': return 'Algo salió mal';
			case 'response.error.badRequest': return 'Solicitud incorrecta';
			case 'response.error.notAuthorized': return 'No autorizado';
			case 'response.error.forbidden': return 'Prohibido';
			case 'response.error.badGateway': return 'Puerta de enlace incorrecta';
			case 'response.messageStatus.unauthorizedUser': return 'Usuario no autorizado';
			case 'response.messageStatus.resourceNotFound': return 'Recurso no encontrado';
			case 'response.messageStatus.internalError': return 'Error interno';
			case 'response.messageStatus.itemDuplicated': return 'Registro ya existe';
			case 'response.messageStatus.incorrectCredentials': return 'Credenciales incorrectas';
			case 'response.messageStatus.theSelectedEquipmentIsInactive': return 'El Equipo seleccionado se encuentra inactivo';
			case 'response.messageStatus.theSelectedSubEquipmentIsInactive': return 'El Sub-Equipo seleccionado se encuentra inactivo';
			case 'response.messageStatus.theOfferIsNoLongerAvailable': return 'La oferta ya no está disponible';
			case 'response.messageStatus.unidentifiedError': return 'Error no identificado';
			default: return null;
		}
	}
}
