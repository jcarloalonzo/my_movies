import '../../../generated/translations.g.dart';

class HttpFailure {
  HttpFailure(this.message);
  final String? message;
}

String handleMessageStatusCode(int id) {
  var textMessage = texts.response.messageStatus;
  switch (id) {
    case 401:
      return textMessage.unauthorizedUser;
    case 404:
      return textMessage.resourceNotFound;
    case 500:
      return textMessage.internalError;
    case 501:
      return textMessage.itemDuplicated;
    case 502:
      return textMessage.incorrectCredentials;
    case 503:
      return textMessage.theSelectedEquipmentIsInactive;
    case 504:
      return textMessage.theSelectedSubEquipmentIsInactive;
    case 505:
      return textMessage.theOfferIsNoLongerAvailable;
    default:
      return textMessage.unidentifiedError;
  }
}
