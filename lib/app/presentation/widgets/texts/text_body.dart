import 'package:flutter/material.dart';

class TextBody extends StatelessWidget {
  const TextBody(
    this.text, {
    super.key,
    this.textAlign = TextAlign.start,
    this.color = Colors.black87,
    this.maxLines = 2,
    this.fontWeight = FontWeight.w400,
    this.size = 13.5,
  });
  final String text;
  final TextAlign textAlign;
  final Color? color;
  final int maxLines;
  final FontWeight fontWeight;
  final double size;
  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      maxLines: maxLines,
      style: Theme.of(context)
          .textTheme
          .bodyMedium
          ?.copyWith(color: color, fontWeight: fontWeight, fontSize: size),
      // style: const TextStyle(fontSize: 15),
      textAlign: textAlign,
      overflow: TextOverflow.ellipsis,
    );
  }
}
