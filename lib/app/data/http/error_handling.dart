import 'dart:io';

import 'package:dio/dio.dart';

import '../../../generated/translations.g.dart';

class DioExceptions implements Exception {
  DioExceptions.fromDioError(DioException dioError) {
    switch (dioError.type) {
      case DioExceptionType.cancel:
        message = textError.theRequestToTheApiServerWasCanceled;
        break;
      case DioExceptionType.connectionTimeout:
        message = textError.connectionTimeoutWithApiServer;
        break;
      case DioExceptionType.receiveTimeout:
        message = textError.receiveTimeoutInConnectionWithApiServer;
        break;
      case DioExceptionType.badResponse:
        message = _handleError(
          dioError.response?.statusCode,
          dioError.response?.data,
        );
        break;
      case DioExceptionType.sendTimeout:
        message = textError.sendTimeoutInConnectionWithApiServer;
        break;
      case DioExceptionType.connectionError:
        message = textError.noInternet;
        break;
      case DioExceptionType.unknown:
        if (dioError.error is SocketException) {
          message = textError.noInternet;
          break;
        }
        message = textError.unexpectedErrorOcurred;
        break;
      default:
        message = textError.somethingWentWrong;
        break;
    }
  }
  final textError = texts.response.error;

  late String message;

  String _handleError(int? statusCode, dynamic error) {
    switch (statusCode) {
      case 400:
        return textError.badRequest;
      case 401:
        return textError.notAuthorized;
      case 403:
        return textError.forbidden;
      case 404:
        return error['mensaje'];
      case 500:
        return error['mensaje'];
      case 502:
        return textError.badGateway;
      default:
        return textError.somethingWentWrong;
    }
  }

  @override
  String toString() => message;
}
