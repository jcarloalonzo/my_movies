import 'package:flutter/material.dart';

import '../../../../generated/translations.g.dart';
import '../../../config/constants/constants.dart';
import '../../../data/entities/response/movie_response.dart';
import '../../../data/entities/response/trending_response.dart';
import '../../widgets/global/Carrousel_slide_movies.dart';
import '../../widgets/global/gridview_movies.dart';
import '../../widgets/texts/text_sub_title.dart';

class MoviesBody extends StatelessWidget {
  const MoviesBody(
      {super.key, required this.popularMovies, required this.trendingMovies});

  final List<Movie> popularMovies;
  final List<TrendingMovie> trendingMovies;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(0.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const SizedBox(height: 40),
          // Carrousel trending movies
          Padding(
            padding: const EdgeInsets.only(
                left: Constants.horizontalPaddingScaffold),
            child: TexTitle(texts.label.trending),
          ),
          const SizedBox(height: 10),
          CarrouselSlideMovies(trendingMovies: trendingMovies),
          const SizedBox(height: 20),
          Padding(
            padding: const EdgeInsets.only(
                left: Constants.horizontalPaddingScaffold),
            child: TexTitle(texts.label.popular),
          ),
          const SizedBox(height: 10),
          // GridView popular movies
          GridViewMovies(popularMovies: popularMovies),
        ],
      ),
    );
  }
}
