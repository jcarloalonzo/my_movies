# My Movies APP

## Versión Flutter 3.13.9

### Features :

1. Firebase RTDB
2. MultiLanguage ( By default it's the cell phone language)
3. Secure Storage
4. Dio
5. Clean Architecture
6. Dependency Injection
7. Carousel
8. Freezed
9. Splash Screen and Icon
10. Animations
11. Router (Go router)
12. Environment variable (DotEnv)

### Get Started

Setps to compile the application - dev.

1. Rename the .env.template file to .env and put the user token and url api.

2. To generate translations file, run the command:

```
dart run slang
```

3. To generate freezed files, run the coomand

```
dart run build_runner build --delete-conflicting-outputs
```

## ![app](app.png)

## ![database_empty](database_empty.png)

![database_changed](database_changed_from_phone.png)
