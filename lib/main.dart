import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';

import 'app/config/constants/environment.dart';
import 'app/my_app.dart';
import 'app/presentation/blocs/cubit/session_cubit.dart';
import 'dependency_injection.dart' as di;
import 'firebase_options.dart';
import 'generated/translations.g.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(options: DefaultFirebaseOptions.currentPlatform);
  // initialize environment variables}
  await Environment.init();
  // Firebase.initializeApp();
  LocaleSettings.useDeviceLocale();
  //
  Intl.defaultLocale = LocaleSettings.currentLocale.languageCode;
  // Set default orientation app
  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown,
  ]);
  // Initialize dependency injections
  di.init();
  runApp(
    BlocProvider(
        create: (context) => di.getIt<SessionCubit>(),
        child: TranslationProvider(child: const MyApp())),
  );
}
