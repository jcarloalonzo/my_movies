import 'package:flutter/material.dart';

import '../../../../generated/translations.g.dart';
import '../../../config/constants/constants.dart';
import '../../../data/entities/response/movie_response.dart';
import '../../widgets/texts/text_body.dart';
import '../../widgets/texts/text_sub_title.dart';

class ContentDetailPage extends StatelessWidget {
  const ContentDetailPage({super.key, required this.movie});
  final Movie movie;

  static const String route = '/routeDetail';

  @override
  Widget build(BuildContext context) {
    final Ids ids = movie.ids;
    return Scaffold(
      appBar: AppBar(
        title: Text(movie.title),
      ),
      body: Padding(
        padding: const EdgeInsets.all(Constants.horizontalPaddingScaffold),
        child: Column(
          children: <Widget>[
            TexTitle(texts.label.year),
            TextBody(movie.year.toString(), fontWeight: FontWeight.w600),
            const Divider(),
            const TexTitle('Track'),
            TextBody(ids.trakt.toString(), fontWeight: FontWeight.w600),
            const Divider(),
            const TexTitle('Slug'),
            TextBody(ids.slug.toString(), fontWeight: FontWeight.w600),
            const Divider(),
            const TexTitle('Imdb'),
            TextBody(ids.imdb.toString(), fontWeight: FontWeight.w600),
            const Divider(),
            const TexTitle('Tmdb'),
            TextBody(ids.tmdb.toString(), fontWeight: FontWeight.w600),
          ],
        ),
      ),
    );
  }
}
