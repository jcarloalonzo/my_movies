import 'package:intl/intl.dart';

class Helpers {
  Helpers._();

  /// The method returns a formatted string number
  ///
  /// Takes an integer and returns a formatted String depends on the type of format
  /// ```dart
  /// int number = 1 200;
  /// compactNumber(number) returns '1.2K'
  /// ```
  static String compactNumber(int number, [int decimals = 0]) {
    final formattedNumber = NumberFormat.compactCurrency(
            decimalDigits: decimals, symbol: '', locale: 'en')
        .format(number);

    return formattedNumber;
  }
}
