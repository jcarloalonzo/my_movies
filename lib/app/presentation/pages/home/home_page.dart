import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../dependency_injection.dart';
import '../../../../generated/translations.g.dart';
import '../../blocs/movies/movies_bloc.dart';
import '../movies/movies_page.dart';
import '../profile/profile_page.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});
  static const String route = '/homePage';

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  late int currentIndex;

  // List of views in home page
  final pages = const <Widget>[
    MoviesPage(),
    ProfilePage(),
  ];

  @override
  void initState() {
    super.initState();
    // Initialize currentIndex
    currentIndex = 0;
  }

  void setIndex(int index) {
    setState(() => currentIndex = index);
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => getIt<MoviesBloc>()..init(),
      child: WillPopScope(
        onWillPop: () async => false,
        child: Scaffold(
          body: IndexedStack(index: currentIndex, children: pages),
          bottomNavigationBar: BottomNavigationBar(
            onTap: setIndex,
            backgroundColor: Colors.grey[100],
            currentIndex: currentIndex,
            elevation: 0,
            items: [
              BottomNavigationBarItem(
                icon: const Icon(Icons.movie_filter_outlined),
                label: texts.label.movies,
              ),
              BottomNavigationBarItem(
                icon: const Icon(Icons.person_2),
                label: texts.label.profile,
              ),
            ],
          ),
          //
        ),
      ),
    );
  }
}
