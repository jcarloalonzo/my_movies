import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:go_router/go_router.dart';

import '../../../config/constants/palette.dart';
import '../../../data/entities/response/movie_response.dart';
import '../../pages/content_detail/content_detail_page.dart';

class GridViewMovies extends StatelessWidget {
  const GridViewMovies({
    super.key,
    required this.popularMovies,
  });
  final List<Movie> popularMovies;

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: GridView.builder(
        gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
            maxCrossAxisExtent: 200,
            childAspectRatio: 3 / 2,
            crossAxisSpacing: 20,
            mainAxisSpacing: 20),
        itemCount: popularMovies.length,
        itemBuilder: (context, index) {
          final Movie movie = popularMovies[index];
          return _CardMovie(
            movie: movie,
            onTap: () {
              context.push(ContentDetailPage.route, extra: movie);
            },
          );
        },
      ),
    );
  }
}

class _CardMovie extends StatelessWidget {
  const _CardMovie({required this.movie, required this.onTap});
  final Movie movie;
  final VoidCallback onTap;
  @override
  Widget build(BuildContext context) {
    final TextTheme textTheme = Theme.of(context).textTheme;
    return GestureDetector(
      onTap: onTap,
      child: Card(
        shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(12)),
            side: BorderSide(color: Palette.primaryColor, width: 0.5)),
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                movie.title,
                style: textTheme.bodyMedium?.copyWith(
                  color: Palette.primaryColor,
                  fontSize: 15,
                  fontWeight: FontWeight.w600,
                ),
                maxLines: 3,
                overflow: TextOverflow.ellipsis,
              ),

              Text(
                movie.year.toString(),
                style: textTheme.bodyMedium?.copyWith(
                  color: Palette.primaryColor,
                  fontSize: 12,
                  fontWeight: FontWeight.w400,
                ),
                maxLines: 3,
                overflow: TextOverflow.ellipsis,
              ),

              //
              //
              const Spacer(),

              // Package flutter_rating_bar
              // Rating bar Widget
              // The service does not provide it
              Align(
                alignment: Alignment.bottomRight,
                child: RatingBarIndicator(
                  rating: 4,
                  itemBuilder: (context, index) => const Icon(
                    Icons.star,
                    color: Colors.amber,
                  ),
                  itemCount: 5,
                  itemSize: 12.0,
                  direction: Axis.horizontal,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
