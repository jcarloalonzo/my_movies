import 'package:google_sign_in/google_sign_in.dart';

import '../../domain/models/user.dart';

class UserMappers {
  /// * Convert the google object to User
  static User googleToUser(GoogleSignInAccount account) {
    return User(
      name: account.displayName ?? '',
      email: account.email,
      photoUrl: account.photoUrl ?? '',
    );
  }
}
