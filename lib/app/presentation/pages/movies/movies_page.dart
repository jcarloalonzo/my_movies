import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../blocs/movies/movies_bloc.dart';
import '../../widgets/state_widgets/error_screen.dart';
import '../../widgets/state_widgets/loading_screen.dart';
import 'movies_body.dart';

class MoviesPage extends StatelessWidget {
  const MoviesPage({super.key});

  @override
  Widget build(BuildContext context) {
    // MoviesPage is listen to changes of state in MoviesBloc
    final state = context.watch<MoviesBloc>().state;
    return Scaffold(
      body: state.when(
        loading: () => const LoadingScreen(),
        failed: (_) => ErrorScreen(_, onRetry: context.read<MoviesBloc>().init),
        loaded: (popularMovies, trendingMovies) => MoviesBody(
          popularMovies: popularMovies,
          trendingMovies: trendingMovies,
        ),
      ),
    );
  }
}
