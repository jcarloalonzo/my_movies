import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../generated/translations.g.dart';
import '../../../config/constants/constants.dart';
import '../../../domain/models/user.dart';
import '../../blocs/cubit/session_cubit.dart';
import '../../widgets/global/custom_buttom.dart';
import '../../widgets/global/my_textfield.dart';

class ProfileBody extends StatefulWidget {
  const ProfileBody({super.key});

  @override
  State<ProfileBody> createState() => _ProfileBodyState();
}

class _ProfileBodyState extends State<ProfileBody> {
  final TextEditingController _usernameController = TextEditingController();
  final TextEditingController _emailController = TextEditingController();
  @override
  void initState() {
    super.initState();
    _init();
  }

  // set the current user in the global state in the textEditing controllers
  _init() {
    final User? user = context.read<SessionCubit>().state.user;
    _usernameController.text = user?.name ?? '';
    _emailController.text = user?.email ?? '';
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(
          horizontal: Constants.horizontalPaddingScaffold),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          // const SizedBox(height: 20),
          context.select((SessionCubit cubit) {
            final User? user = cubit.state.user;
            if (user?.photoUrl != null) {
              return CircleAvatar(
                radius: 60,
                backgroundColor: Colors.red,
                child: ClipOval(
                  child: Image.network(
                    user!.photoUrl!,
                    fit: BoxFit.fill,
                  ),
                ),
              );
            }
            return const SizedBox();
          }),
          const SizedBox(height: 40),
          MyTextField(
            controller: _usernameController,
          ),
          const SizedBox(height: 12),
          MyTextField(
            controller: _emailController,
          ),
          const SizedBox(height: 40),
          // buttom
          CustomButtom(
            title: texts.label.update,
            onTap: _onTapButtom,
          ),
        ],
      ),
    );
  }

  void _onTapButtom() {
    FocusScope.of(context).unfocus();
    final sessionCubit = context.read<SessionCubit>();
    // get user from sessioncubit
    final User userSession = sessionCubit.state.user!;

    final String names = _usernameController.text;
    final String email = _emailController.text;

    // Create an object User with the current user update
    final User userUpdate = userSession.copyWith(name: names, email: email);

    sessionCubit.updateUser(userUpdate);
  }
}
