part of 'movies_bloc.dart';

abstract class MoviesEvent {
  const MoviesEvent();
}

class MoviesStateChanged extends MoviesEvent {
  MoviesStateChanged(this.state);
  final MoviesState state;
}
