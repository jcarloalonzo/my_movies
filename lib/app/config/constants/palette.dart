import 'package:flutter/material.dart';

class Palette {
  Palette._();

  static const Color primaryColor = Color(0XFF0B1E3A);

  static const Color black = Color.fromARGB(255, 0, 0, 0);
}
