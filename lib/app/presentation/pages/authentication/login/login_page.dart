import 'package:flutter/material.dart';

import '../../../../config/constants/palette.dart';
import 'login_body.dart';

class LoginPage extends StatelessWidget {
  const LoginPage({super.key});
  static const String route = '/loginPage';

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false,
      child: const Scaffold(
        backgroundColor: Palette.primaryColor,
        body: LoginBody(),
      ),
    );
  }
}
