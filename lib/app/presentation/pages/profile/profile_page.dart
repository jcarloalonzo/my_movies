import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';

import '../../blocs/cubit/session_cubit.dart';
import '../authentication/login/login_page.dart';
import 'profile_body.dart';

class ProfilePage extends StatelessWidget {
  const ProfilePage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        actions: [
          IconButton(
              onPressed: () => _onTapLogout(context),
              icon: const Icon(
                Icons.logout,
                color: Colors.red,
                size: 25,
              )),
        ],
      ),
      body: const ProfileBody(),
    );
  }

  _onTapLogout(BuildContext context) async {
    // sign out app
    await context.read<SessionCubit>().signOut();
    if (!context.mounted) return;
    // Go to login
    context.pushReplacement(LoginPage.route);
  }
}
