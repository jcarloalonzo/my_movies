import 'package:go_router/go_router.dart';

import '../../data/entities/response/movie_response.dart';
import '../../presentation/pages/content_detail/content_detail_page.dart';
import '../../presentation/pages/pages.dart';

/// Route app with [go_router] package
final GoRouter appRouter = GoRouter(
  initialLocation: '/',
  routes: [
    GoRoute(
      path: '/',
      builder: (context, state) => const SplashPage(),
    ),
    GoRoute(
      path: LoginPage.route,
      builder: (context, state) => const LoginPage(),
    ),
    GoRoute(
      path: HomePage.route,
      builder: (context, state) => const HomePage(),
    ),
    GoRoute(
      path: ContentDetailPage.route,
      builder: (context, state) {
        Movie movie = state.extra as Movie;

        // The page requires a Movie object
        return ContentDetailPage(movie: movie);
      },
    ),
  ],
);
