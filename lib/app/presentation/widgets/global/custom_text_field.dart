import 'package:flutter/material.dart';

import '../../../config/constants/palette.dart';

class CustomTextField extends StatelessWidget {
  const CustomTextField({
    super.key,
    this.onSubmit,
    this.onChanged,
    this.onTap,
    this.controller,
    this.textInputType = TextInputType.text,
    this.maxLines,
    this.hintText,
    this.isEnabled = true,
    this.obscureText = false,
    this.textAlign = TextAlign.start,
    this.textInputAction,
    this.autoFocus = false,
    this.textCapitalization = TextCapitalization.none,
  });

  final Function(String)? onSubmit, onChanged;
  final Function()? onTap;
  final TextEditingController? controller;
  final TextInputType textInputType;
  final int? maxLines;
  final String? hintText;
  final bool isEnabled, obscureText;
  final TextAlign textAlign;
  final TextInputAction? textInputAction;
  final bool autoFocus;
  final TextCapitalization textCapitalization;

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      onTap: onTap,
      autofocus: autoFocus,
      controller: controller,
      enabled: isEnabled,
      textInputAction: textInputAction,
      onFieldSubmitted: onSubmit,
      textAlign: textAlign,

      keyboardType: textInputType,
      textCapitalization: textCapitalization,
      onChanged: onChanged,
      obscureText: obscureText,
      textAlignVertical: TextAlignVertical.center,
      maxLines: maxLines ?? 1,
      cursorColor: Palette.primaryColor,
      style: Theme.of(context).textTheme.bodyMedium?.copyWith(fontSize: 16),
      decoration: InputDecoration(
        hintStyle: Theme.of(context).textTheme.bodyMedium?.copyWith(
              color: Palette.primaryColor,
              fontWeight: FontWeight.w300,
              fontSize: 16,
            ),
        isDense: true,
        // errorText: 'ssd',
        hintText: hintText,
        border: InputBorder.none,
        filled: true,
        fillColor: Colors.transparent,
      ),
      //
    );
  }
}
