import '../../domain/datasources/session_datasource.dart';
import '../../domain/models/user.dart';
import '../services/remote/google_signin_service.dart';

class GoogleDatasourceImpl implements SessionDatasource {
  GoogleDatasourceImpl({required this.googleSignInService});

  final GoogleSignInService googleSignInService;

  @override
  Future<User?> getCurrentUser() {
    return googleSignInService.getCurrentUser();
  }

  @override
  Future<User?> signIn() {
    return googleSignInService.signIn();
  }

  @override
  Future<void> signOut() async {
    await googleSignInService.signOut();
  }

  @override
  Future<void> updateUser(User user) async {
    await googleSignInService.updateUser(user);
  }

  @override
  Future<bool> existsToken() {
    return googleSignInService.existsToken();
  }
}
