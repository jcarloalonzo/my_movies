import 'package:flutter/material.dart';

import '../../../config/constants/palette.dart';

class CustomButtom extends StatelessWidget {
  const CustomButtom({
    super.key,
    this.onTap,
    required this.title,
    this.colorButtom = Palette.primaryColor,
  });

  final VoidCallback? onTap;
  final String title;
  final Color colorButtom;

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
        style: ElevatedButton.styleFrom(
          elevation: 4,
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
          padding: const EdgeInsets.all(11),
          backgroundColor: colorButtom,
          shadowColor: Colors.black,
          // foregroundColor: Palette.secondaryColor.withOpacity(0.8),
          minimumSize: const Size(100, 45), //////// HERE
          maximumSize: const Size(150, 45), //////
        ),
        onPressed: onTap,
        child: Text(
          title,
          style: const TextStyle(
              color: Colors.white, fontWeight: FontWeight.w600, fontSize: 16),
        ));
  }
}
