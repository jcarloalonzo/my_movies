import '../models/user.dart';

abstract class SessionDatasource {
  Future<User?> signIn();
  Future<User?> getCurrentUser();
  Future<void> updateUser(User user);
  Future<void> signOut();
  Future<bool> existsToken();
}
