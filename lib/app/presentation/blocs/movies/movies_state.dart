import 'package:freezed_annotation/freezed_annotation.dart';

import '../../../data/entities/response/movie_response.dart';
import '../../../data/entities/response/trending_response.dart';

part 'movies_state.freezed.dart';

@freezed
class MoviesState with _$MoviesState {
  factory MoviesState.loading() = MoviesStateLoading;
  factory MoviesState.failed([String? failure]) = MoviesStateFailed;
  factory MoviesState.loaded({
    required List<Movie> popularMovies,
    required List<TrendingMovie> trendingMovies,
  }) = MoviesStateLoaded;
}
