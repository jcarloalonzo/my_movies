import '../../domain/datasources/session_datasource.dart';
import '../../domain/models/user.dart';
import '../../domain/repositories/session_repository.dart';

class SessionRepositoryImpl implements SessionRepository {
  SessionRepositoryImpl({required this.sessionDatasource});

  final SessionDatasource sessionDatasource;
  @override
  Future<User?> signIn() {
    return sessionDatasource.signIn();
  }

  @override
  Future<User?> getCurrentUser() {
    return sessionDatasource.getCurrentUser();
  }

  @override
  Future<void> signOut() async {
    await sessionDatasource.signOut();
  }

  @override
  Future<void> updateUser(User user) async {
    await sessionDatasource.updateUser(user);
  }

  @override
  Future<bool> existsToken() {
    return sessionDatasource.existsToken();
  }
}
